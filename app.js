// Require packages
const fetch = require("node-fetch")
const cheerio = require("cheerio")
// Declare Json file to store the scrape data in
const UnetJson = {
    "cards": []
}
// Fetch HTML page from Inet
fetch("https://www.inet.se/kategori/164/grafikkort-gpu")
.then((resp)  => resp.text())
.then((text) => scrapeWebpage(text) )

// This function scrapes the data from the Inet page and
// stores it into a JSON object.
// Note: The Inet page only fetches the first 10 objects
// without the use of client-side javascript
function scrapeWebpage(page) {
    // Load the HTML page into cherrio as a selector
    const selector = cheerio.load(page)
    // Find the list in which the graphic cards are stored in
    // and loop through the 10 objects
    selector("body").find("div[class='product-list product-list-size-default']").find('ul').find("li")
    .each((i,elem) => {
        // Declare an empty JSON object
        let card = {}
    
        // Find the description string from the element and
        // scrape the information in it
        let description = selector(elem).find(".product-text").find("a").text().split(/[|:]/)
        card.speed = description[1].trim()
        card.memory = description[2].trim()
        card.watt = description[4].trim()
        card.length  = description[6].trim()

        // Get information that is more spread out
        card.price = selector(elem).find("span[class='price']").text().trim()
        card.name = selector(elem).find(".product-text").find("h4").text()
        card.stock = selector(elem).find("div[class='stock']").text().trim()
        card.picture = selector(elem).find("picture").find("img").attr("src").substring(2)

        // Add the graphic card info into the cards array
        UnetJson.cards.push(card)
    }) 
}

// Require to use express 
const express = require("express")
const path = require("path")
// Start a new express app
const app = express()
// Default to port 8080
const {PORT = 8080} = process.env
// Setup middleware
app.use(express.static(path.join(__dirname,"static")))
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Define the '/data' end-point, delivering the graphic card
// json object
app.get("/data", (req,ress) => {
    console.log("Getting data")
    ress.status(200).json(UnetJson)
})


// Start the app at the port
app.listen(PORT, () => {
    console.log(`Listing on port ${PORT}`)
})