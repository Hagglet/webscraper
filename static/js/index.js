
// Fetch data from the data end-point and call
// the "fillInInformation" function
fetch("/data")
.then((resp) => resp.json()) 
.then((resp) => fillInInformaton(resp))
    

// This function takes the graphic card json object
// and makes a list in HTML
function fillInInformaton(json) {
    // Declare index used to alterate list color
    let index = 0

    // Make a HTML row for every array object
    json.cards.forEach(card => {
        // Add row with the information in the appropriate placeholder
        document.getElementById("Unetlist").innerHTML += `
        <div class="row" style="background-color: ${index%2 == 0 ? "lightgray" : "#EEEEEE"};">
            <div class="col-2 p-2">
                <img alt="Test" src="images/gtx1.jpg" width="100px">
            </div>
            <div class="col-6 p-2">
                <h4>${card.name}</h4>
                <p>GPU-frekvens: ${card.speed} | ${card.memory} | Rek. watt (dator): ${card.watt} | Längd: ${card.length}</p>
            </div>
            <div class="col-2 p-2 d-flex justify-content-center">
                <p class="align-self-center"><span class="dot"></span> ${card.stock}</p>
            </div>
            <div class="col-2 p-2 text-center mt-auto">
                <h4>${card.price}</h4>
                <button type="Button" class="btn btn-success">Buy</button>
            </div>
        </div>
        `
        // Increment the altering color index
        index++
    });
} 